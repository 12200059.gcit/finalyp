<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthManager;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\ExaminerController;
use App\Http\Controllers\GuideController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::group(['middleware'=> 'auth'], function(){

    Route::get('/', [ProjectController::class, 'index'])->name("home");

    // student 
    Route::get('student/studentDashboard', [ProjectController::class, 'viewStudent'])->name('studentDashboard');
    Route::get('student/studentDashboard/{id}', [StudentController::class, 'viewStudent'])->name('student.studentDashboard');

    Route::get('student/viewMarks/{id}/{ca}', [StudentController::class, 'viewMark1'])->name('viewMark1');


    // guide 
    Route::get('guide/guideDashboard', [GuideController::class, 'viewGuide'])->name('guide.guideDashboard');
    Route::get('/getRubric/{ca}', [GuideController::class, 'getCARubric'])->name("rubric.ca");
    Route::post('/postmark/{id}', [GuideController::class, 'postMark'])->name('postMark');

  
  

// examiner 
    Route::get('examiner/examinerDashboard', [ExaminerController::class, 'viewExaminer'])->name('examinerDashboard');
    Route::get('examiner/examinerpage', [ExaminerController::class, 'examiner'])->name('examinerpage');
      // Rubrics
      Route::get('/getRubrics/{ca}', [ExaminerController::class, 'getCARubrics'])->name("rubrics.ca");
      Route::post('/postmarks/{id}', [ExaminerController::class, 'postMarks'])->name('postMarks');



    Route::get('/addStudents/{id}', [ProjectController::class, 'addStd'])->name("addStudents");
    Route::get('/addSupervisor/{id}', [ProjectController::class, 'addSpv'])->name("addSupervisor");
    Route::get('/addExaminer/{id}', [ProjectController::class, 'addExm'])->name("addExaminar");


  


    Route::get('/Projectgroup/{course}',[ProjectController::class, 'projectgroup'])->name('Createprojectgroup');

    Route::get('/CreateEvalution',[ProjectController::class, 'evaluation'])->name('CreateEvalution');
    Route::get('/archive/{id}',[ProjectController::class, 'getArchive'])->name('getArchive');
    Route::get('/archive',[ProjectController::class, 'archive'])->name('archive');
    Route::get('/archive',[ProjectController::class, 'archivepage'])->name('archiveProject');
    Route::get('/Cadetails/{ca}',[ProjectController::class, 'caDetails'])->name('caDetailpage');
    Route::get('/download/{file_path}', [ProjectController::class, 'download'])->name('downloadpage');
    Route::get('/projectcategorize',[ProjectController::class, 'projectcartegoised'])->name('projectcartegorize');



    




    Route::get('/Createproject',[ProjectController::class, 'create'])->name('Createproject');
    
    Route::post('/Createproject', [ProjectController::class, 'store'])->name('projectsdetail.store');

    Route::get('/Assignproject', [ProjectController::class, 'assignproject'])->name('AssignProject');
    Route::get('/addRubrics', [ProjectController::class, 'rubrics'])->name('MarkingRubrics');
    Route::post('/addRubrics', [ProjectController::class, 'addRubrics'])->name('postRubrics');
    Route::post('/Assignproject/{id}/addsupervisor', [ProjectController::class, 'addsupervisor'])->name('AssignProject.addsupervisor');
    Route::post('/Assignproject/{id}/addstudent', [ProjectController::class, 'addStudents'])->name('AssignProject.addstudent');
    Route::post('/Assignproject/{id}/addExaminer', [ProjectController::class, 'addExaminers'])->name('AssignProject.addExaminer');

});


// Route::middleware(['auth','role:student'])->group(function () {
//     Route::get('student/studentDashboard', [StudentController::class, 'viewStudent'])->name('studentDashboard');
//     Route::get('student/viewMarks', [StudentController::class, 'viewMark1'])->name('viewMark1');
//     Route::get('student/studentProfile', [StudentController::class, 'stdProfile'])->name('stdProfile');
//     Route::get('student/submission', [StudentController::class, 'docSubmission'])->name('docSubmission');
// });




Route::get('/login', [AuthManager::class, 'login'])->name("login");
Route::post('/login', [AuthManager::class, 'loginpost'])->name("login.post");
Route::get('/signup', [AuthManager:: class, "signup"])->name("signup");
Route::post('/signup', [AuthManager::class, 'signuppost'])->name("signup.post");
Route::get('/db', function () {
    return view('dbconnection');
});

Route::get('/logout', [AuthManager::class, 'logout'])->name("logout");