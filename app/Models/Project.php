<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;
    
    public function supervisor(){
        return $this ->hasmany(Supervisor::class);
    }

    protected $fillable = [
        'course',
        'projectgroup_no',
        'project_title',
        'product_owner',
        'file_path',
        'archived'
        // Add 'projectgroup_no' to the fillable array
    ];
}
