<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Examiner extends Model
{
    use HasFactory,Notifiable;
    public function project(){
        return $this ->belongsTo(Project::class);
    }
    protected $fillable = [
    
        // Add 'projectgroup_no' to the fillable array
        'project_id',
        'name',
        'email',
    ];
}
