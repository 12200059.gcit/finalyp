<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Marks;
use RealRashid\SweetAlert\Facades\Alert;

class StudentController extends Controller
{
    public function viewStudent($id){
        $user = Auth::user();
        $id = $id;
        return view('student.studentDashboard',compact('id','user'));
    }

    public function viewMark1($id,$ca){
        $marks = Marks::where('ca', $ca)->where('project_id', $id)->first();

        if (!$marks) {
            return redirect()->route('student.studentDashboard',compact('id'))->with('error', 'Marks not found');
        }else{
            $filePath = public_path('assets/' . $marks->file);        
            return response()->download($filePath, 'CA.pdf');
        }

       
    }

}
