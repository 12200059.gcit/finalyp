<?php
namespace App\http\Controllers;
use App\Models\User;
use App\Models\Project;
use App\Models\Students;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;

use Illuminate\Support\Facades\Session;

class AuthManager extends Controller
{
    protected $table = 'users';

    function login(){
        if(Auth::check()){
            if(session('role')==='admin'){
                Alert::success("congrats",'You have login successfully');
                return view('home');
            }
            if(session('role')==='student'){
                return view('student.dashboard');
            }
            if(session('role')==='examiner'){
                return view('examiner.examinerDashboard');
            }
            if(session('role')==='supervisor'){
                return view('guide.guideDashboard');
            }
        } else{
                return view("login");
            }
    }

    function signup(){
        return view("signup");
    }

    public function loginpost(Request $request){
    $request->validate([
        "email" => 'required',
        "password" => 'required'
    ]);

    $credentials = $request->only("email", "password");
    $user = User::where('email', $request->email)->first();

    if (Auth::attempt($credentials)) {

        if ($user) {
            // Check user roles and redirect accordingly
            if ($user->role === 'admin') {
                Session::put('role', 'admin');
                Alert::success("congrats",'You have login successfully');
                    return redirect()->intended(route("home"))->with("success", "Login successfully");
            } elseif ($user->role === 'student') {
                Session::put('role', 'student');
                $id = $user->project_id;
                return redirect()->route("student.studentDashboard",['id'=>$id])->with("success", "Login successfully");
            } elseif ($user->role === 'examiner') {
                Session::put('role', 'examiner');
                $products = Project::where('id', $user->project_id)->first();
                $members= Students::where('project_id', $user->project_id)->get();   
                return view("examiner.examinerDashboard", compact('products','members'))->with("success", "Login successfully");
            }
            elseif ($user->role === 'supervisor') {
                Session::put('role', 'supervisor');
                return redirect()->route("guide.guideDashboard")->with("success", "Login successfully");

            } else {
                // Handle other roles if needed
                return redirect(route("login"))->with("error", "Invalid role for login");
            }
        } else {
            // Handle the case when $user is not found
            return redirect(route("login"))->with("error", "User with this email does not exist.");
        }
    }

    if (!$user) {
        return redirect(route("login"))->with("error", "User with this email does not exist.");
    }
    return redirect(route("login"))->with("error", "Login details are not correct");
}


    
    function signuppost(Request $request){

        $request ->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required'
        ]);
        $data['name'] = $request ->name;
        $data['email'] = $request ->email;
        $data['password'] = Hash::make($request ->password);
        $user = User::create($data);
        if(!$user){
            return redirect(route('signup'))->with("error","signup failed, try again.");
        }
        return view('login')->with("success",'signup successful');
    }

    function logout(){
        Auth::logout();
        return redirect(route('login'));
    }
}
