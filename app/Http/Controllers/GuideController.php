<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Project;
use App\Models\Rubrics;
use App\Models\Marks;
use App\Models\Students;

class GuideController extends Controller
{

    public function postMark(Request $request,$id){
        $request->validate([
            'ca' => 'required',
            'file' => 'required|file|mimes:pdf',
        ]);
    
        // Store the uploaded file in the 'assets' directory
        $file = $request->file('file');
        $filename = time() . '.' . $file->getClientOriginalName();
        $file->move('assets', $filename);
    
        // Construct the file path
        $filePath = 'assets/' . $filename;
    
        // Create a new project and save it to the database
        Marks::create([
            'project_id'=>$id,
            'ca' => $request->ca,
            'file' => $filename,
        ]);
        // Alert::success("congrats",'You have added the rubrics successfully');

        $user = Auth::user();
        $products = Project::where('id', $user->project_id)->first();   
        $members= Students::where('project_id', $user->project_id)->get(); 
        return view('guide.guideDashboard',compact('user','products','members'))->withSuccess('You have successfully added the marking rubrics');
    }

    function getCARubric($ca){
        $rubrics = Rubrics::where('CA', $ca)->first();
        return view('guide.rubrics', compact('rubrics'));
    }


    public function viewGuide(){
        $user = Auth::user();
        $products = Project::where('id', $user->project_id)->first();
        $members= Students::where('project_id', $user->project_id)->get();
        return view('guide.guideDashboard', compact('user','products','members'));  
      
       }

    public function rubrics(){
        $user = Auth::user();
        return view('guide.guideEvaluationRubrics', ['user' => $user]);
       }

    public function guide(){
        $user = Auth::user();
        return view('guide.guidepage', ['user' => $user]);
       }
    
    public function profile(){
        $user = Auth::user();
        return view('guide.guideSetting', ['user' => $user]);
       }

       
}
