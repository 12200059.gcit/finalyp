<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Project;
use App\Models\Rubrics;
use App\Models\Students;
use App\Models\Marks;
use RealRashid\SweetAlert\Facades\Alert;

class ExaminerController extends Controller
{
    public function postMarks(Request $request,$id){
        $request->validate([
            'ca' => 'required',
            'file' => 'required|file|mimes:pdf',
        ]);
    
        // Store the uploaded file in the 'assets' directory
        $file = $request->file('file');
        $filename = time() . '.' . $file->getClientOriginalName();
        $file->move('assets', $filename);
    
        // Construct the file path
        $filePath = 'assets/' . $filename;
    
        // Create a new project and save it to the database
        Marks::create([
            'project_id'=>$id,
            'ca' => $request->ca,
            'file' => $filename,
        ]);
        
       

        $user = Auth::user();
        $products = Project::where('id', $user->project_id)->first();   
        $members= Students::where('project_id', $user->project_id)->get(); 
        Alert::success("congrats",'You have added CA marks successfully');
        
        return view('examiner.examinerDashboard',compact('user','products','members'))->withSuccess('You have successfully added the marking rubrics');
    }
    
    public function viewExaminer(){
        $user = Auth::user();
        $products = Project::where('id', $user->project_id)->first(); 
           
        return view('examiner.examinerDashboard', compact('user','products'));
       }

       
    function getCARubrics($ca){
        $rubrics = Rubrics::where('CA', $ca)->first();
        return view('examiner.CARubrics', compact('rubrics'));
    }

}
