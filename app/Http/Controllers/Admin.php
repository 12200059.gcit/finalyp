<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;



class Admin extends Controller
{
    public function construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $user_id = Auth::user()->id;
        return view("home",compact('user_id'));
    }
}
?>