<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Project;
use App\Models\Supervisor;
use App\Notifications\AnnoucementCreated;
use Illuminate\Support\Facades\Mail;
use App\Mail\SupervisorInvite;
use App\Models\Students;
use App\Models\Examiner;
use Illuminate\Support\Facades\Session;
use App\Models\Rubrics;
use Illuminate\Support\Facades\Stroage;
use RealRashid\SweetAlert\Facades\Alert;
use App\Models\User;
use Illuminate\Support\Facades\Hash;


class ProjectController extends Controller
{

    public function viewStudent(){
        return view('student.studentDashboard');
    }


    public function getArchive($id){
        $archived_project = Project::where('id', $id)->first();
        $project = Project::where('archived', true)->get();

        if (!$archived_project) {
            Alert::success('The project is archived already');
            Session::flash('error', 'Project not found');
        } else {
            // Update the 'archived' attribute to true
            $archived_project->archived = true;
            $archived_project->save();
            Alert::success("congrats",'You have archived the project successfully');

            Session::flash('success', 'Project archived successfully');
        }
       

        return view('archive',compact('project'));
    }
    public function archivepage(){
        $project = Project::where('archived', true)->get();
        return view('archive',compact('project'));
    }

    public function addRubrics(Request $request)
    {
        $request->validate([
            'CA' => 'required',
            'file' => 'required|file|mimes:pdf',
        ]);
    
        // Store the uploaded file in the 'assets' directory
        $file = $request->file('file');
        $filename = time() . '.' . $file->getClientOriginalName();
        $file->move('assets', $filename);
    
        // Construct the file path
        $filePath = 'assets/' . $filename;
    
        // Create a new project and save it to the database
        Rubrics::create([
            'CA' => $request->CA,
            'file_path' => $filename,
        ]);
        Alert::success("congrats",'You have added the rubrics successfully');

    
        return redirect()->route('MarkingRubrics')->withSuccess('You have successfully added the marking rubrics');
    }
    

   public function caDetails($ca){
    $rubrics = Rubrics::where('CA', $ca)->get();
    return view('CAdetails', compact('rubrics'));  
    }


   public function download(Request $request, $file_path)
   {
       return response()->download(public_path('assets/'. $file_path));
   }


    public function index(){
        $products = Project::get();
        $projectCount = Project::count();
        $archivedProjectsCount = Project::where('archived', true)->count();
        return view('home', compact('products','projectCount','archivedProjectsCount'));
    }

    public function projectgroup($course){
        $products = Project::where('course', $course)->get();
        return view('Projectgroup',compact('products'));
    }
    

    public function assignproject(){
        $supervisors = Supervisor::get();
        $student = Students::get();
        $examiner = Examiner::get();
        $products = Project::get();
       

        return view('AssignProject',compact('products','supervisors','student','examiner'));
    }
    
    public function create(){
        return view('Createproject');

    }
    public function evaluation(){
        return view('CreateEvalution');
    }

    public function rubrics(){
        return view("addRubrics");
    }

   

    public function store(Request $request){
        $request->validate([
            'course' => 'required',
            'projectgroup_no' => 'required',
            'project_title' => 'required',
            'product_owner' => 'required',
            'file' => 'required|file|mimes:jpeg,png,pdf',
        ]);

    // Store the uploaded file in the storage/app/public directory
        $file = $request->file('file');
        $filePath = $file->store('public');

    // Create a new project and save it to the database
        Project::create([
            'course' =>$request->course,
            'projectgroup_no' => $request->projectgroup_no,
            'project_title' => $request->project_title,
            'product_owner' => $request->product_owner,
            'file_path' => $filePath,
        ]);

        Alert::success("congrats",'You have create the project successfully');

        return redirect()->route('Createproject')->withSuccess('You have successfully created the project');
    }

    public function projectcartegoised(){
        $courses = Project::get();
        return view('projectcartegorize',compact('courses'));
    }



    public function addSupervisor(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
        ]);
        // Check if an entry already exists for the specified project
        $existingSupervisor = User::where('project_id', $id)->first();
        $Supervisor = Supervisor::where('project_id', $id)->first();

        $password = 'hello123';

        if ($existingSupervisor && $Supervisor) {
            // If an entry exists, update the existing entry instead of creating a new one
            $existingSupervisor->update([
                'name' => $request->name,
                'email' => $request->email,
                'password'=>Hash::make($password),
                'role' =>'supervisor',
                'project_id'=>$id,
            ]);
            $Supervisor->update([
                'project_id'=>$id,
                'name' => $request->name,
                'email' => $request->email,
            ]);
    
            Alert::success("congrats", 'Supervisor information updated successfully');
        } else {
            // If no entry exists, create a new one
            $data = Project::findOrFail($id);
    
            $announcement =User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password'=>Hash::make($password),
                'role' =>'supervisor',
                'project_id'=>$id,
            ]);

            $Supervisor = Supervisor::create([
                'project_id'=>$id,
                'name' => $request->name,
                'email' => $request->email,
            ]);
    
            // Send an email to the newly created Supervisor
            Mail::to($request->email)->send(new SupervisorInvite($data));
    
            Alert::success("congrats", 'You have added the Supervisor successfully');
        }
    
        return redirect()->route('AssignProject')->withSuccess('You have successfully created the project');
    }
    
    
    //notification
    public function addStudents(Request $request, $id){
        $request->validate([
            'name' => 'required',
            'email' => 'required',
        ]);

        $data = Project::where('id', $id)->first();
        $password = 'hello123';

        $announcement = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password'=>Hash::make($password),
            'role' =>'student',
            'project_id'=>$id,
        ]);

        $students = Students::create([
            'project_id'=>$id,
            'name' => $request->name,
            'email' => $request->email,
            
        ]);
         // Send an email to the newly created Supervisor
        Mail::to($request->email)->send(new SupervisorInvite($data));

        Alert::success("congrats",'You have added the member successfully');

        // $announcement->notify(new AnnoucementCreated($announcement));
        return redirect()->route('AssignProject')->withSuccess('You have successfully created the project');
    }


    public function addExaminers(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
        ]);
    
        // Check if an entry already exists for the specified project
        $existingExaminer = User::where('project_id', $id)->first();
        $examiner = Examiner::where('project_id', $id)->first();

        $password = 'hello123';
        if ($existingExaminer && $examiner) {
            // If an entry exists, update the existing entry instead of creating a new one
            $existingExaminer->update([
                'name' => $request->name,
                'email' => $request->email,
                'password'=>Hash::make($password),
                'role' =>'examiner',
                'project_id'=>$id,
            ]);

            $examiner->update([
                'project_id'=>$id,
                'name' => $request->name,
                'email' => $request->email,
            ]);
    

            Alert::success("congrats", 'Examiner information updated successfully');
        } else {
            // If no entry exists, create a new one
            $data = Project::findOrFail($id);
    
            $announcement = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password'=>Hash::make($password),
                'role' =>'examiner',
                'project_id'=>$id,
            ]);

            $examiner = Examiner::create([
                'project_id'=>$id,
                'name' => $request->name,
                'email' => $request->email,
            ]);
    
            // Send an email to the newly created Examiner
            Mail::to($request->email)->send(new SupervisorInvite($data));
    
            Alert::success("congrats", 'You have added the examiner successfully');
        }
    
        return redirect()->route('AssignProject')->withSuccess('You have successfully created the project');
    }
    

    public function addStd($id){
        return view('addStudent',compact('id'));
    }

    public function addSpv($id){
        return view('addSupervisor',compact('id'));
    }

    public function addExm($id){
        return view('addExaminar',compact('id'));
    }
   
   
   
}
