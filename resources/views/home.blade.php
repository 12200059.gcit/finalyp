
@if(session('role') === 'admin')
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
      crossorigin="anonymous"
    />
    <title>Document</title>
  </head>
  <body>
    <div class="container-fluid">
      @csrf
      <div class="row" style="background-color: #f2edf3">
        <div
          class="col-sm-3"
          style="
            border: 2px solid white;
            border-top-right-radius: 30px;
            border-bottom-right-radius: 30px;
            position: relative;
            max-height: 100%;
            max-width: auto;
            justify-content: center;
            align-items: center;
            display: flex;
            flex-direction: column;
            background-color: white;
            box-shadow: 10px 0 5px rgba(0, 0, 0, 0.2);
          "
        >
          <div
            class="d-flex flex-column"
            style="align-items: center; text-align: center"
          >
            <div style="max-width: 100px; height: auto; margin-top: 15%">
              <img
                src="./image/profile.png"
                alt="profileimage"
                style="max-width: 100%; height: auto"
              />
            </div>
                @auth
                <p>Welcome <br>{{ auth()->user()->name }}</p>
                @else
                <p>User not found</p>
                <a href="/login">Login</a>
                <p>Login in</p>
                @endauth
                <h5>Manager</h5>
          </div>

          <div
            class="d-flex flex-row"
            style="align-items: center; justify-content: center"
          >
            <div
              class="d--lg-flex flex-column"
              style="align-items: center; justify-content: center"
            >
              <a
              href="{{route('home')}}"
                style="
                  display: inline-block;
                  padding: 10px 40px;
                  background-color: #ff7900;
                  color: #fff;
                  text-decoration: none;
                  border-radius: 5px;
                  display: flex;
                  justify-content: space-around;
                  align-items: center;
                  margin: 10%;
                "
                ><img
                  src="./image/dashboard.png"
                  alt="createproject"
                  style="
                    max-width: 100%;
                    height: auto;
                    width: 40px;
                    border-radius: 10px;
                  "
                />
                <h5
                  style="
                    color: #149634;
                    font-size: 15px;
                    font-weight: bold;
                    margin: 0;
                  "
                >
                  Dashboard
                </h5>
              </a>
            </div>
          </div>

          <div
            style="
              align-items: center;
              justify-content: center;
              padding-top: 10%;
            "
          >
          <div class="d--lg-flex flex-column" style="align-items: center; justify-content: center; padding: 10px; text-align: center;">
              <img src="./image/createicon.png" alt="createproject" style="max-width: 100%; height: auto; width: 30px; border-radius: 10px;">
              <a href="{{route('Createproject')}}" style="display: inline-block; padding: 5px 10px; color: #ff7900; text-decoration: none; border-radius: 5px; transition: background-color 0.3s;" onmouseover="this.style.backgroundColor='#ff7900'; this.style.color='#fff';" onmouseout="this.style.backgroundColor=''; this.style.color='#ff7900';">
                  <p style="font-size: 16px; color: #149634; margin: 0;">Create Project</p>
              </a>
          </div>


            <div
              class="d--lg-flex flex-column"
              style="
                align-items: center;
                justify-content: center;
                padding: 10px;
                text-align: center;
              "
            >
              <img
                src="./image/assign.png"
                alt="createproject"
                style="
                  max-width: 100%;
                  height: auto;
                  width: 30px;

                  border-radius: 10px;
                "
              />
              <a
              href="{{route('AssignProject')}}"
              style="display: inline-block; padding: 5px 10px; color: #ff7900; text-decoration: none; border-radius: 5px; transition: background-color 0.3s;" onmouseover="this.style.backgroundColor='#ff7900'; this.style.color='#fff';" onmouseout="this.style.backgroundColor=''; this.style.color='#ff7900';"
                ><p
                  style="
                    font-size: 16px;
                    color: #149634;

                    margin: 0;
                  "
                >
                  Assign Project
                </p></a
              >
            </div>
            <div
              class="d--lg-flex flex-column"
              style="
                align-items: center;
                justify-content: center;
                padding: 10px;
                text-align: center;
              "
            >
              <img
                src="./image/group.png"
                alt="createproject"
                style="
                  max-width: 100%;
                  height: auto;
                  width: 30px;

                  border-radius: 10px;
                "
              />
              <a
              href="{{route('projectcartegorize')}}"
              style="display: inline-block; padding: 5px 10px; color: #ff7900; text-decoration: none; border-radius: 5px; transition: background-color 0.3s;" onmouseover="this.style.backgroundColor='#ff7900'; this.style.color='#fff';" onmouseout="this.style.backgroundColor=''; this.style.color='#ff7900';"
                ><p
                  style="
                    font-size: 16px;
                    color: #149634;

                    margin: 0;
                  "
                >
                  Project Group
                </p></a
              >
            </div>
            <div
              class="d--lg-flex flex-column"
              style="
                align-items: center;
                justify-content: center;
                padding: 10px;
                text-align: center;
              "
            >
              <img
                src="./image/createi.png"
                alt="createproject"
                style="
                  max-width: 100%;
                  height: auto;
                  width: 30px;

                  border-radius: 10px;
                "
              />
              <a
              href="{{route('CreateEvalution')}}"
              style="display: inline-block; padding: 5px 10px; color: #ff7900; text-decoration: none; border-radius: 5px; transition: background-color 0.3s;" onmouseover="this.style.backgroundColor='#ff7900'; this.style.color='#fff';" onmouseout="this.style.backgroundColor=''; this.style.color='#ff7900';"
                ><p
                  style="
                    font-size: 16px;
                    color: #149634;

                    margin: 0;
                  "
                >
                  Create Evaluation
                </p></a
              >
            </div>
            <div
              class="d--lg-flex flex-column"
              style="
                align-items: center;
                justify-content: center;
                padding: 10px;
                text-align: center;
              "
            >
              <img
                src="./image/archive.png"
                alt="createproject"
                style="
                  max-width: 100%;
                  height: auto;
                  width: 30px;

                  border-radius: 10px;
                "
              />
              <a
              href="{{route('archiveProject')}}"
              style="display: inline-block; padding: 5px 10px; color: #ff7900; text-decoration: none; border-radius: 5px; transition: background-color 0.3s;" onmouseover="this.style.backgroundColor='#ff7900'; this.style.color='#fff';" onmouseout="this.style.backgroundColor=''; this.style.color='#ff7900';"
                ><p
                  style="
                    font-size: 16px;
                    color: #149634;

                    margin: 0;
                  "
                >
                  Archive Project
                </p></a
              >
            </div>
          </div>
          <div style="padding-top: 10%; display: flex">
            <div
              class="d--lg-flex flex-column"
              style="
                align-items: center;
                justify-content: center;
                padding: 10px;
                text-align: center;
              "
            >
              <img
                src="./image/setting.png"
                alt="createproject"
                style="
                  max-width: 100%;
                  height: auto;
                  width: 20px;

                  border-radius: 10px;
                "
              />
              <a
                href="./Profile.html"
                style="display: inline-block; padding: 5px 10px; color: #ff7900; text-decoration: none; border-radius: 5px; transition: background-color 0.3s;" onmouseover="this.style.backgroundColor='#ff7900'; this.style.color='#fff';" onmouseout="this.style.backgroundColor=''; this.style.color='#ff7900';"
                ><p style="font-size: 16px; color: #149634; margin: 0">
                  Setting
                </p></a
              >
            </div>
            <div
              class="d--lg-flex flex-column"
              style="
                align-items: center;
                justify-content: center;
                padding: 10px;
                text-align: center;
              "
            >
              <img
                src="./image/logout.png"
                alt="createproject"
                style="
                  max-width: 100%;
                  height: auto;
                  width: 20px;

                  border-radius: 10px;
                "
              />
              <a
                href="{{route('logout')}}"
                style="display: inline-block; padding: 5px 10px; color: #ff7900; text-decoration: none; border-radius: 5px; transition: background-color 0.3s;" onmouseover="this.style.backgroundColor='#ff7900'; this.style.color='#fff';" onmouseout="this.style.backgroundColor=''; this.style.color='#ff7900';"
                ><p
                  style="
                    font-size: 16px;
                    color: #149634;

                    margin: 0;
                  "
                >
                  Log out
                </p></a
              >
             
            </div>
          </div>
          <div style="margin-top: 20px; text-align: center; background-color: #ff7900; padding: 10px; border-radius: 5px; border-bottom-right-radius: 20px;">
              <h5>College Information</h5>
              <p>Gyalpozhing College of Information Technology</p>
              <p>Royal University of Bhutan</p>
              <p>Chamjekha, Thimphu, Bhutan</p>
              <hr>
              <p style="color: #fff;">Copyright © Gyalpozhing College of Information Technology 2023. All Rights Reserved.</p>
          </div>

        </div>
        <div class="col-sm-9">
          <div>
            <div class="d-flex flex-row" style="margin: 4%">
              <div
                class="d--lg-flex flex-column"
                style="align-items: center; justify-content: center"
              >
                <img
                  src="./image/logo.png"
                  alt="createproject"
                  style="
                    max-width: 100%;
                    height: auto;
                    width: 70px;
                    /* background-color: #149634; */
                    border-radius: 10px;
                  "
                />

                <a
                  href="#"
                  style="display: inline-block; padding: 5px 10px; color: #ff7900; text-decoration: none; border-radius: 5px; transition: background-color 0.3s;" onmouseover="this.style.backgroundColor='#ff7900'; this.style.color='#fff';" onmouseout="this.style.backgroundColor=''; this.style.color='#ff7900';"
                  > <h5
                  style="
                    color: #149634;
                    font-size: 20px;
                    font-weight: bold;
                    margin: 5px;
                  "
                >
                  Dashboard
                </h5></a
                >
              </div>
            </div>
            <div>
              <div class="container">
                <div class="row">
                  <div
                    class="col"
                    style="
                      border: 2px solid whitesmoke;
                      margin: 3%;
                      border-radius: 10px;
                      max-width: 20%;
                      max-height: auto;
                      height: 10%;
                      background-color: #149634;
                    "
                  >
                    <div
                      class="d-lg-flex flex-row"
                      style="
                        justify-content: space-between;
                        align-items: center;
                      "
                    >
                      <h5 style="text-align: center; margin: 0; color: white">
                      <p style="font-size: 16px; line-height: 1.5;">Projects:<br>{{ $projectCount }}</p>

                      <!-- Inline CSS for screens with a width of 600 pixels or less -->
                      <!-- <p style="font-size: 14px; line-height: 1.3; @media (max-width: 600px) { font-size: 14px; }">
                        Projects: <br>{{ $projectCount }}
                      </p> -->

                      </h5>

                      <img src="./image/profile.png" style="width: 50%; max-width: 100%; height: auto;" alt="Profile Image"/>
                    </div>
                  </div>
                  <div
                    class="col"
                    style="
                      border: 2px solid whitesmoke;
                      margin: 3%;
                      border-radius: 10px;
                      max-width: 20%;
                      max-height: auto;
                      height: 10%;
                      background-color: #ff7900;
                    "
                  >
                    <div
                      class="d-lg-flex flex-row"
                      style="
                        justify-content: space-between;
                        align-items: center;
                      "
                    >
                      <h5 style="text-align: center; margin: 0; color: white">
                      <p style="font-size: 16px; line-height: 1.5;">Archived:<br>{{ $archivedProjectsCount }}</p>
                      </h5>
                      
                      <img src="./image/archive.png" style="width: 50%; max-width: 100%; height: auto;" alt="archive Image"/>
                      <style>
    /* Default styles */
  

                        /* Responsive styles for screens with a maximum width of 767 pixels */
                        @media (max-width: 767px) {
                          .d-lg-flex {
                            flex-direction: column;
                            align-items: center;
                           
                          }

                          img {
                            width: 100%;
                          }
                        }
                      </style>
                     
                     
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div>
              <div
                class="container"
                style="
                  border: 2px solid white;
                  margin-top: 5%;
                  border-radius: 10px;
                  background-color: white;
                  box-shadow: 10px 10px 5px 5px rgba(0, 0, 0, 0.2);
                "
              >
                <div style="margin: 3%">
                  <h4>Project Status</h4>
                </div>

                <div
                  class="container"
                  style="margin-top: 5%; margin-bottom: 2%"
                >

               
                <table class="table">
                  <thead style="background-color: #000000;">
                    <tr style="color: white;">
                      <th scope="col">SI.no</th>
                      <th scope="col">Project title</th>
                      <th scope="col">Archive</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($products as $product)
                      <tr>
                        <th scope="row">{{ $loop->index+1 }}</th>
                        <td>{{ $product->project_title }}</td>
                        <td> 
                            @if($product->archived)
                              Yes
                            @else
                              No
                             @endif
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
@else
<div>Page not found!</div>
@endif