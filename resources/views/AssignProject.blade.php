
@if(session('role') === 'admin')
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
      crossorigin="anonymous"
    />
    <title>Document</title>
  </head>
  <body>
    <div class="container-fluid">
    @csrf
      <div class="row" style="background-color: #f2edf3">
        <div
          class="col-sm-3"
          style="
            border: 2px solid white;
            border-top-right-radius: 30px;
            border-bottom-right-radius: 30px;
            position: relative;
            max-height: 100%;
            max-width: auto;
            justify-content: center;
            align-items: center;
            display: flex;
            flex-direction: column;
            background-color: white;
            box-shadow: 10px 0 5px rgba(0, 0, 0, 0.2);
          "
        >
          <div
            class="d-flex flex-column"
            style="align-items: center; text-align: center"
          >
            <div style="max-width: 100px; height: auto; margin-top: 15%">
              <img
                src="./image/profile.png"
                alt="profileimage"
                style="max-width: 100%; height: auto"
              />
            </div>
                @auth
                <p>Welcome <br>{{ auth()->user()->name }}</p>
                @else
                <p>User not found</p>
                <a href="/login">Login</a>
                <p>Login in</p>
                @endauth
                <h5>Manager</h5>
          </div>

          <div
            class="d-flex flex-row"
            style="align-items: center; justify-content: center"
          >
            <div
              class="d--lg-flex flex-column"
              style="align-items: center; justify-content: center"
            >
              <a
              href="{{route('home')}}"
                style="
                  display: inline-block;
                  padding: 10px 40px;
                  background-color: #ff7900;
                  color: #fff;
                  text-decoration: none;
                  border-radius: 5px;
                  display: flex;
                  justify-content: space-around;
                  align-items: center;
                  margin: 10%;
                "
                ><img
                  src="./image/dashboard.png"
                  alt="createproject"
                  style="
                    max-width: 100%;
                    height: auto;
                    width: 40px;
                    border-radius: 10px;
                  "
                />
                <h5
                  style="
                    color: #149634;
                    font-size: 15px;
                    font-weight: bold;
                    margin: 0;
                  "
                >
                  Dashboard
                </h5>
              </a>
            </div>
          </div>

          <div
            style="
              align-items: center;
              justify-content: center;
              padding-top: 10%;
            "
          >
          <div class="d--lg-flex flex-column" style="align-items: center; justify-content: center; padding: 10px; text-align: center;">
              <img src="./image/createicon.png" alt="createproject" style="max-width: 100%; height: auto; width: 30px; border-radius: 10px;">
              <a href="{{route('Createproject')}}" style="display: inline-block; padding: 5px 10px; color: #ff7900; text-decoration: none; border-radius: 5px; transition: background-color 0.3s;" onmouseover="this.style.backgroundColor='#ff7900'; this.style.color='#fff';" onmouseout="this.style.backgroundColor=''; this.style.color='#ff7900';">
                  <p style="font-size: 16px; color: #149634; margin: 0;">Create Project</p>
              </a>
          </div>


            <div
              class="d--lg-flex flex-column"
              style="
                align-items: center;
                justify-content: center;
                padding: 10px;
                text-align: center;
              "
            >
              <img
                src="./image/assign.png"
                alt="createproject"
                style="
                  max-width: 100%;
                  height: auto;
                  width: 30px;

                  border-radius: 10px;
                "
              />
              <a
              href="{{route('AssignProject')}}"
              style="display: inline-block; padding: 5px 10px; color: #ff7900; text-decoration: none; border-radius: 5px; transition: background-color 0.3s;" onmouseover="this.style.backgroundColor='#ff7900'; this.style.color='#fff';" onmouseout="this.style.backgroundColor=''; this.style.color='#ff7900';"
                ><p
                  style="
                    font-size: 16px;
                    color: #149634;

                    margin: 0;
                  "
                >
                  Assign Project
                </p></a
              >
            </div>
            <div
              class="d--lg-flex flex-column"
              style="
                align-items: center;
                justify-content: center;
                padding: 10px;
                text-align: center;
              "
            >
              <img
                src="./image/group.png"
                alt="createproject"
                style="
                  max-width: 100%;
                  height: auto;
                  width: 30px;

                  border-radius: 10px;
                "
              />
              <a
              href="{{route('projectcartegorize')}}"
              style="display: inline-block; padding: 5px 10px; color: #ff7900; text-decoration: none; border-radius: 5px; transition: background-color 0.3s;" onmouseover="this.style.backgroundColor='#ff7900'; this.style.color='#fff';" onmouseout="this.style.backgroundColor=''; this.style.color='#ff7900';"
                ><p
                  style="
                    font-size: 16px;
                    color: #149634;

                    margin: 0;
                  "
                >
                  Project Group
                </p></a
              >
            </div>
            <div
              class="d--lg-flex flex-column"
              style="
                align-items: center;
                justify-content: center;
                padding: 10px;
                text-align: center;
              "
            >
              <img
                src="./image/createi.png"
                alt="createproject"
                style="
                  max-width: 100%;
                  height: auto;
                  width: 30px;

                  border-radius: 10px;
                "
              />
              <a
              href="{{route('CreateEvalution')}}"
              style="display: inline-block; padding: 5px 10px; color: #ff7900; text-decoration: none; border-radius: 5px; transition: background-color 0.3s;" onmouseover="this.style.backgroundColor='#ff7900'; this.style.color='#fff';" onmouseout="this.style.backgroundColor=''; this.style.color='#ff7900';"
                ><p
                  style="
                    font-size: 16px;
                    color: #149634;

                    margin: 0;
                  "
                >
                  Create Evaluation
                </p></a
              >
            </div>
            <div
              class="d--lg-flex flex-column"
              style="
                align-items: center;
                justify-content: center;
                padding: 10px;
                text-align: center;
              "
            >
              <img
                src="./image/archive.png"
                alt="createproject"
                style="
                  max-width: 100%;
                  height: auto;
                  width: 30px;

                  border-radius: 10px;
                "
              />
              <a
              href="{{route('archiveProject')}}"
              style="display: inline-block; padding: 5px 10px; color: #ff7900; text-decoration: none; border-radius: 5px; transition: background-color 0.3s;" onmouseover="this.style.backgroundColor='#ff7900'; this.style.color='#fff';" onmouseout="this.style.backgroundColor=''; this.style.color='#ff7900';"
                ><p
                  style="
                    font-size: 16px;
                    color: #149634;

                    margin: 0;
                  "
                >
                  Archive Project
                </p></a
              >
            </div>
          </div>
          <div style="padding-top: 10%; display: flex">
            <div
              class="d--lg-flex flex-column"
              style="
                align-items: center;
                justify-content: center;
                padding: 10px;
                text-align: center;
              "
            >
              <img
                src="./image/setting.png"
                alt="createproject"
                style="
                  max-width: 100%;
                  height: auto;
                  width: 20px;

                  border-radius: 10px;
                "
              />
              <a
                href="./Profile.html"
                style="display: inline-block; padding: 5px 10px; color: #ff7900; text-decoration: none; border-radius: 5px; transition: background-color 0.3s;" onmouseover="this.style.backgroundColor='#ff7900'; this.style.color='#fff';" onmouseout="this.style.backgroundColor=''; this.style.color='#ff7900';"
                ><p style="font-size: 16px; color: #149634; margin: 0">
                  Setting
                </p></a
              >
            </div>
            <div
              class="d--lg-flex flex-column"
              style="
                align-items: center;
                justify-content: center;
                padding: 10px;
                text-align: center;
              "
            >
              <img
                src="./image/logout.png"
                alt="createproject"
                style="
                  max-width: 100%;
                  height: auto;
                  width: 20px;

                  border-radius: 10px;
                "
              />
              <a
                href="{{route('logout')}}"
                style="display: inline-block; padding: 5px 10px; color: #ff7900; text-decoration: none; border-radius: 5px; transition: background-color 0.3s;" onmouseover="this.style.backgroundColor='#ff7900'; this.style.color='#fff';" onmouseout="this.style.backgroundColor=''; this.style.color='#ff7900';"
                ><p
                  style="
                    font-size: 16px;
                    color: #149634;

                    margin: 0;
                  "
                >
                  Log out
                </p></a
              >
             
            </div>
          </div>
          <div style="margin-top: 20px; text-align: center; background-color: #ff7900; padding: 10px; border-radius: 5px; border-bottom-right-radius: 20px;">
              <h5>College Information</h5>
              <p>Gyalpozhing College of Information Technology</p>
              <p>Royal University of Bhutan</p>
              <p>Chamjekha, Thimphu, Bhutan</p>
              <hr>
              <p style="color: #fff;">Copyright © Gyalpozhing College of Information Technology 2023. All Rights Reserved.</p>
          </div>

        </div>
        <div class="col-sm-9">
          <div>
            <div>
            @if($message = Session::get("success"))
              @include('sweetalert::alert')
            @endif
            <!-- @include('sweetalert::alert') -->
             
            <div>
              <div
                class="container"
                style="
                  border: 2px solid white;
                  margin-top: 5%;
                  border-radius: 10px;
                  background-color: white;
                  box-shadow: 10px 10px 5px 5px rgba(0, 0, 0, 0.2);
                "
              >
                <div class="d-flex flex-row" style="margin: 3%; justify-content: space-between;">
                  <h4>Assign Project</h4>
                 
                </div>

                <div
                  class="table-responsive"
                  style="margin-top: 5%; margin-bottom: 2%"
                >
                <table class="table">
                  <thead style="background-color: #000000;">
                    <tr style="color: white;">
                      <th scope="col">SI.no</th>
                      <th scope="col">Project title</th>
                      <th scope="col">Product owner</th>
                      <th scope="col">Supervisior</th>
                      <th scope="col">Members</th>
                      <th scope="col">Examiner</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($products as $product)
                    <tr data-id="{{$product->id}}">
                      <tr>
                        <th scope="row">{{ $loop->index+1 }}</th>
                        <td>{{ $product->project_title }}</td>
                        <td>{{ $product->product_owner }}</td>
                        <td>
                          @foreach ($supervisors as $supervisor)
                            @if($product->id === $supervisor->project_id)
                            <div>
                              <span>{{$supervisor->name}}</span><br>
                              <span>{{$supervisor->email}}</span>
                            </div>
                            @endif
                          @endforeach
                        </td>
                        <td>
                        @foreach ($student as $students)
                            @if($product->id === $students->project_id)
                            <div>
                              <span>{{$students->name}}</span><br>
                              <span>{{$students->email}}</span>
                            </div>
                            @endif
                          @endforeach
                        </td>
                        <td>
                        @foreach ($examiner as $examiners)
                            @if($product->id === $examiners->project_id)
                            <div>
                              <span>{{$examiners->name}}</span><br>
                              <span>{{$examiners->email}}</span>
                            </div>
                            @endif
                          @endforeach
                        </td>
                      </tr>
                      <tr>
                        <th scope="row"></th>
                        <td></td>
                        <td></td>
                        <td>
                        <a
                      href="{{route('addSupervisor',['id'=>$product->id])}}"
                      style="
                        display: inline-block;

                   
                        color: black;
                        text-decoration: none;
                       
                      "
                      >+ Add Supervisior</a
                    ></td>
                        <div
                  class="d--lg-flex flex-column"
                  style="
                    align-items: center;
                    justify-content: center;
                    /* padding: 5px; */
                  "
                >
                </div>
                        </td>
                        <td>
                        <a
                      href="{{route('addStudents',['id'=>$product->id])}}"
                      style="
                        display: inline-block;

                   
                        color: black;
                        text-decoration: none;
                       
                      "
                      >+ Add Students</a
                    ></td>
                        <div
                  class="d--lg-flex flex-column"
                  style="
                    align-items: center;
                    justify-content: center;
                    /* padding: 5px; */
                  "
                >
                

                  
                </div>
                    </td>
                    <td>
                        <a
                      href="{{route('addExaminar',['id'=>$product->id])}}"
                      style="
                        display: inline-block;

                   
                        color: black;
                        text-decoration: none;
                       
                      "
                      >+ Add Examiner</a
                    ></td>
                        <div
                  class="d--lg-flex flex-column"
                  style="
                    align-items: center;
                    justify-content: center;
                    /* padding: 10px; */
                  "
                >
                </div>
                    </td>
              </tr>
              </tr>
                    
                    @endforeach
                  </tbody>
                </table>
                    <div style="margin-left: 45%">
                      <button
                        type="submit"
                        class="btn"
                        style="margin: 2%; background-color: #ff7900"
                      >
                        View More
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script>
                    var isHTMLCodeVisible = false;

                    function toggleCode() {
                      var htmlContainer =
                        document.getElementById("htmlContainer");

                      if (isHTMLCodeVisible) {
                        // Hide the HTML code by setting the container to an empty string
                        htmlContainer.innerHTML = "";
                      } else {
                        // Show the HTML code
                        var htmlCode = `
                          <div >
                            @if($message = Session::get("success"))
                              <div class = "alert alert-success alert-block">
                              <strong>{{$message}}</strong>
                              </div>
                            @endif
                            <form method="post" action="{{ route('AssignProject.addstudent',['id'=>$product->id]) }}" >
                            <h4>Add Students</h4>
                            @csrf
                              <div class="mb-3">
                                <label class="form-label">Name:</label>
                                <input type="text" class="form-control" name='name' id='name' required>
                              </div>
                              <div class="mb-3">
                                <label class="form-label">Email:</label>
                                <input type="text" class="form-control" name='email' id='email' required>
                              </div>
                              <button type="submit" class="btn" style="background-color:#149634">Add</button>
                              <button type="submit" class="btn" style="background-color:#149634"><a href="{{route('AssignProject')}}">Cancel</button>
                            </form>
                          </div>
                        `;
                        htmlContainer.innerHTML = htmlCode;
                      }

                      // Toggle the visibility flag
                      isHTMLCodeVisible = !isHTMLCodeVisible;
                    }

                    // function Confirmation() {
                    //   var confirmed = window.confirm(
                    //     "Are you sure you want to add supervisor to the group?"
                    //   );
                    //   if (confirmed) {
                    //     alert("You have added supervisor to the group successfully.");
                    //     window.location.href="{{route('AssignProject')}}";
                    //     // Perform your action here if 'Yes' is clicked
                    //   } else {
                    //     // Redirect to another page if 'No' is clicked
                    //     window.location.href="{{route('AssignProject')}}";
                    //   }
                    // }


                    function toggleHTMLCode(id) {
                      var id = id;
                      var htmlContainer =
                        document.getElementById("htmlContainer");

                      if (isHTMLCodeVisible) {
                        // Hide the HTML code by setting the container to an empty string
                        htmlContainer.innerHTML = "";
                      } else {
                        // Show the HTML code
                        var htmlCode = `
                          <div >
                            @if($message = Session::get("success"))
                              <div class = "alert alert-success alert-block">
                              <strong>{{$message}}</strong>
                              </div>
                            @endif
                            <form method="post" action="{{ route('AssignProject.addsupervisor',['id'=>$product]) }}" >
                            <h4>Add Supervisior</h4>
                            @csrf
                              <div class="mb-3">
                                <label class="form-label">Name:</label>
                                <input type="text" class="form-control" name='name' id='name' required>
                              </div>
                              <div class="mb-3">
                                <label class="form-label">Email:</label>
                                <input type="text" class="form-control" name='email' id='email' required>
                              </div>
                              <button type="submit" class="btn" style="background-color:#149634">Add</button>
                              <button type="submit" class="btn" style="background-color:#149634"><a href="{{route('AssignProject')}}">Cancel</button>
                            </form>
                          </div>
                        `;
                        htmlContainer.innerHTML = htmlCode;
                      }

                      // Toggle the visibility flag
                      isHTMLCodeVisible = !isHTMLCodeVisible;
                    }

                    // function Confirmation() {
                    //   var confirmed = window.confirm(
                    //     "Are you sure you want to add supervisor to the group?"
                    //   );
                    //   if (confirmed) {
                    //     alert("You have added supervisor to the group successfully.");
                    //     window.location.href="{{route('AssignProject')}}";
                    //     // Perform your action here if 'Yes' is clicked
                    //   } else {
                    //     // Redirect to another page if 'No' is clicked
                    //     window.location.href="{{route('AssignProject')}}";
                    //   }
                    // }


                    var isHTMLCodeVisible = false;

                    function toggleCodes() {
                      var htmlContainer =
                        document.getElementById("htmlContainer");

                      if (isHTMLCodeVisible) {
                        // Hide the HTML code by setting the container to an empty string
                        htmlContainer.innerHTML = "";
                      } else {
                        // Show the HTML code
                        var htmlCode = `
                          <div >
                            @if($message = Session::get("success"))
                              <div class = "alert alert-success alert-block">
                              <strong>{{$message}}</strong>
                              </div>
                            @endif
                            <form method="post" action="{{ route('AssignProject.addExaminer',['id'=>$product->id]) }}" >
                            <h4>Add Examiner</h4>
                            @csrf
                              <div class="mb-3">
                                <label class="form-label">Name:</label>
                                <input type="text" class="form-control" name='name' id='name' required>
                              </div>
                              <div class="mb-3">
                                <label class="form-label">Email:</label>
                                <input type="text" class="form-control" name='email' id='email' required>
                              </div>
                              <button type="submit" class="btn" style="background-color:#149634">Add</button>
                              <button type="submit" class="btn" style="background-color:#149634"><a href="{{route('AssignProject')}}">Cancel</button>
                            </form>
                          </div>
                        `;
                        htmlContainer.innerHTML = htmlCode;
                      }

                      // Toggle the visibility flag
                      isHTMLCodeVisible = !isHTMLCodeVisible;
                    }

                    // function Confirmation() {
                    //   var confirmed = window.confirm(
                    //     "Are you sure you want to add supervisor to the group?"
                    //   );
                    //   if (confirmed) {
                    //     alert("You have added supervisor to the group successfully.");
                    //     window.location.href="{{route('AssignProject')}}";
                    //     // Perform your action here if 'Yes' is clicked
                    //   } else {
                    //     // Redirect to another page if 'No' is clicked
                    //     window.location.href="{{route('AssignProject')}}";
                    //   }
                    // }


    </script>
  </body>
</html>
@else
<div>Page not found!</div>
@endif