<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>db connection</title>
</head>
<body>
    <div>
    <?php
    use Illuminate\Support\Facades\DB; // Import the DB facade at the beginning of your PHP file.

    if (DB::connection()->getPdo()) {
        echo "Successfully connected to DB and DB name is " . DB::connection()->getDatabaseName();
    }
    ?>

    </div>
</body>
</html>