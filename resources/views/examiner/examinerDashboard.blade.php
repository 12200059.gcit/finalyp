@if(session('role') === 'examiner')
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Examiner</title>
    <!-- Include Bootstrap CSS here, either locally or via CDN -->
    <!-- Example using CDN -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Include Font Awesome for icons -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" rel="stylesheet">
    <style>
        body {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin-left: -115px;
            background-color: #E8E4E9;
        }

        .container {
            display: flex;
            align-items: stretch;
            margin: 50px;
            background-color: #E8E4E9;
        }

        .sidebar {
            background-color: #fff;
            border-top-right-radius: 40px;
            border-bottom-right-radius: 40px;
            padding: 20px;
            display: flex;
            flex-direction: column;
            align-items: center;
            width: 400px;
        }

        .sidebar ul {
            list-style-type: none;
            padding: 0;
        }

        .sidebar li {
            margin-bottom: 10px;
            padding: 10px;
            border-radius: 20px;
            cursor: pointer;
        }

        .main-content {
            background-color: #E8E4E9;
            padding-left: 20px;
            padding-top: -10px;
            flex: 1;
        }

        .project-status {
            width: 100%;
            background-color: #fff;
            border-radius: 10px;
            padding: 30px;
            display: flex;
            flex-direction: column;
            align-items: center;
        }

        .row {
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            padding: 15px 0;
        }

        /* General styles for the table */
        table {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 20px;
        }

        th,
        td {
            border: 1px solid #ddd;
            padding: 10px;
            text-align: center;
            /* Center align the content */
        }

        th {
            background-color: #000;
            color: #fff;
        }

        /* Styles for the button inside cells */
        button {
            background-color: #149634;
            text-decoration: none;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            border: none;
            border-radius: 8px;
            padding: 8px 10px;
            /* Adjust padding as needed */
            display: block;
            margin: 0 auto;
            /* Center align the button */
        }

        /* Responsive styles using media queries */
        @media (max-width: 768px) {
            .container {
                flex-direction: column;
                align-items: center;
            }

            .sidebar {
                width: 100%;
                border-radius: 0;
                margin-bottom: 20px;
            }

            .main-content {
                padding-left: 0;
            }
        }

        @media (max-width: 600px) {
            th,
            td {
                font-size: 12px;
            }

            button {
                font-size: 12px;
                padding: 5px;
                /* Adjust padding for smaller screens */
            }
        }

        @media (max-width: 400px) {
            th,
            td {
                font-size: 10px;
            }

            button {
                font-size: 10px;
                padding: 3px;
                /* Adjust padding for smaller screens */
            }
        }
    </style>
</head>

<body>

    <div class="container">
        <div class="sidebar">
            @include('examiner.examinerNavbar')
            <div style="margin-top: 20px; text-align: center; background-color: #ff7900; padding: 20px; border-radius: 5px; border-bottom-right-radius: 20px;">
        <h5>College Information</h5>
        <p>Gyalpozhing College of Information Technology</p>
        <p>Royal University of Bhutan</p>
        <p>Chamjekha, Thimphu, Bhutan</p>
        <hr>
        <p style="color: #fff;">Copyright © Gyalpozhing College of Information Technology 2023. All Rights Reserved.</p>
    </div>
        </div>

        <div class="main-content">
            <h5 style="color: #000000; padding-bottom: 20px;"><i class="fas fa-home"></i> Dashboard</h5>
            <div class="project-status">
                <table>
                    <thead>
                        <p>You are assign as examiner for this group<b> {{$products->project_title}}</b></p>
                        <tr>
                            <th>Group</th>
                            <th>Projects title</th>
                            <th>Projects member</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$products->id}}</td>
                            <td>{{$products->project_title}}</td>
                            <td>
                                @foreach($members as $member)
                                    <span>{{$member->name}} </span><br>
                                    <span>{{$member->email}} </span> <br>                          
                                @endforeach
                            </td>
                        </tr>
                    </tbody>
                </table>
                <h5>View the marking Rubrics for individual CA</h5>
                <div class="row">
                    <button><a href="{{ route('rubrics.ca',['ca'=>'CA1']) }}" style="text-decoration: none; color: black; font-weight: bold;">CA1 Rubric</a></button>
                    <button><a href="{{ route('rubrics.ca',['ca'=>'CA2']) }}" style="text-decoration: none; color: black; font-weight: bold;">CA2 Rubric</a></button>
                    <button><a href="{{ route('rubrics.ca',['ca'=>'CA3']) }}" style="text-decoration: none; color: black; font-weight: bold;">CA3 Rubric</a></button>
                    <button><a href="{{ route('rubrics.ca',['ca'=>'CA4']) }}" style="text-decoration: none; color: black; font-weight: bold;">CA4 Rubric</a></button>
                </div>
                @if($message = Session::get("success"))
                  @include('sweetalert::alert')
                @endif

                <div>
                    <form action="{{ route('postMarks',['id'=>$products->id])}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-3">
                            <label class="form-label">CA:</label>
                            <select class="form-select" name="ca" required>
                                <option value="" disabled selected>Select your CA</option>
                                <option value="CA1" {{ old('CA') == 'CA1' ? 'selected' : '' }}>CA1</option>
                                <option value="CA2" {{ old('CA') == 'CA2' ? 'selected' : '' }}>CA2</option>
                                <option value="CA3" {{ old('CA') == 'CA3' ? 'selected' : '' }}>CA3</option>
                                <option value="CA4" {{ old('CA') == 'CA4' ? 'selected' : '' }}>CA4</option>
                            </select>
                        </div>
                        <div class="d--lg-flex flex-column">
                            <img src="../image/upload.png" alt="createproject" width="100" style="border-radius: 10px;">
                            <label for="file">Upload File:</label>
                            <input type="file" name="file" id="file" required>
                        </div>
                        <button type="submit" class="btn" style="background-color: #149634">Add marks</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

</body>

</html>



@else
<div>
    Page not found!
</div>
@endif



