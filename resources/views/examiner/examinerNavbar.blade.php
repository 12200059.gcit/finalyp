@if (auth()->user()->role === 'examiner')

<style>
    nav ul {
    list-style-type: none;
    padding: 0;
    }

    nav ul li {
        display: inline-block;
        margin-right: 10px;
    }

    nav ul li a {
        text-decoration: none;
        color: inherit;
        padding: 5px 10px;
        border-radius: 5px;
        display: flex; /* Add this property to enable flexbox */
        align-items: center; /* Align items vertically in the center */
    }

    nav ul li a .icon {
    margin-right: 10px; /* Adjust margin as needed */
    }

    nav ul li a:hover {
        background-color: #eee;
    }

    nav ul li a.active {
        background-color: #ccc;
    }

</style>
<div class="container" style=" background-color: #fff;
            border-top-right-radius: 40px;
            border-bottom-right-radius: 40px;
            padding: 20px;
            display: flex;
            flex-direction: column;
            align-items: center;
            width: 400px;">
<img
                  src="../image/logo.png"
                  alt="createproject"
                  style="
                    max-width: 100%;
                    height: auto;
                    width: 100px;
                    /* background-color: #149634; */
                    border-radius: 10px;
                  "
                />
<div style="text-align: center; display: flex;
            flex-direction: column;">
    <h5 style="margin-top: 10px;">
    @auth
        <p>Welcome <br>{{ auth()->user()->name }}</p>
    @endauth
    </h5>
        
    <h5>{{ auth()->user()->role }}</h5>
</div>

<nav  style="text-align: center; display: flex;
            flex-direction: column;">
    <ul  style="text-align: center; display: flex;
            flex-direction: column;">
        <li>

            <a href="{{ route('examinerDashboard') }}" class="{{ request()->routeIs('examinerDashboard') ? 'active' : '' }}"><i class="fas fa-home"></i> Dashboard</a></li>    
        <li>
           
                <a href="{{ route('logout') }}"  class="{{ request()->routeIs('examinerDashboard') ? 'active' : '' }}" >
                    <i class="fas fa-sign-out-alt"></i> LogOut
                </a>
        
        </li>
    </ul>
</nav>
</div>



@else
<div>
    The page cannot be accessed!
</div>
@endif