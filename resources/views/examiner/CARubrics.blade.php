
@if(session('role') === 'examiner')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div style="height:100vh">
        <iframe src="{{ asset('assets/' . $rubrics->file_path) }}" width="100%" height="100%"></iframe>
    </div>
</body>
</html>
@else
<div>Page not found!</div>
@endif