<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>

<div class="container" style="max-width: 100%; padding: 0 20px; margin: 30px auto;display: flex; align-items: center; justify-content: center;">

@if($message = Session::get("success"))
  @include('sweetalert::alert')
@endif
  <form action ="{{route('login.post')}}" method= "POST" class="ms-auto me-auto mt-auto" style="width: 100%; max-width: 350px; background-color: white; margin: auto; padding: 20px; border: 1px solid white; box-shadow:10px 10px 10px 10px rgba(0, 0, 0, 0.2);border-radius: 10px;">
  <div style="display: flex; align-items: center; justify-content: center;">
  <img src="./image/logo.png" alt="createproject"style="max-width: 100%;height: auto;width: 300px;border-radius: 10px;"/>

  </div>
  <br>
  
  @csrf
    <div class="mb-3">
      <label  class="form-label">Email address</label>
      <input type="email" class="form-control" name="email" >
      @if ($errors->has('email'))
        <span class="text-danger">{{$errors->first('email')}}</span>
      @endif
    </div>
    <div class="mb-3">
      <label class="form-label">Password</label>
      <input type="password" class="form-control" name="password" >
      @if ($errors->has('password'))
        <span class="text-danger">{{$errors->first('password')}}</span>
      @endif
    </div>
    <button type="submit" class="btn btn-primary">Login</button><br/>
    <div style="padding-top:10px;">
    <a href="#"style=" color: #ff7900; text-decoration: none; " 
                ><p
                  style="
                    font-size: 16px;
                    color: #6495ED;

                    margin: 0;
                  "
                >
                  lost Password?
                </p></a
              >
    </div>
    
    <hr>
    <button type="submit" class="btn">Cookies notice</button><br/>
    
    
  </form>
</div>
</body>
</html>