
@if(session('role') === 'student')
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Student</title>
    <!-- Include Bootstrap CSS here, either locally or via CDN -->
    <!-- Example using CDN -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <!-- Include Font Awesome for icons -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" rel="stylesheet">
    <style>
        body {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin-left: -115px;
            background-color: #E8E4E9;
        }

        .container {
            display: flex;
            align-items: stretch;
            margin: 50px;
            background-color: #E8E4E9;
        }

        .sidebar {
            background-color: #fff;
            border-top-right-radius: 40px;
            border-bottom-right-radius: 40px;
            padding: 20px;
            display: flex;
            flex-direction: column;
            align-items: center;
            width: 400px;
        }

        .sidebar ul {
            list-style-type: none;
            padding: 0;
        }

        .sidebar li {
            margin-bottom: 10px;
            padding: 10px;
            border-radius: 20px;
            cursor: pointer;
        }

        .main-content {
            background-color: #E8E4E9;
            padding-left: 20px;
            padding-top: -10px;
            flex: 1;
        }

        .project-status {
            width: 100%;
            background-color: #fff;
            border-radius: 10px;
            padding: 30px;
            display: flex;
            flex-direction: column;
            align-items: center;
        }

        .row {
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            padding: 15px 0;
        }

        /* General styles for the table */
        table {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 20px;
        }

        th,
        td {
            border: 1px solid #ddd;
            padding: 10px;
            text-align: center;
            /* Center align the content */
        }

        th {
            background-color: #000;
            color: #fff;
        }

        /* Styles for the button inside cells */
        button {
            background-color: #149634;
            text-decoration: none;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            border: none;
            border-radius: 8px;
            padding: 8px 10px;
            /* Adjust padding as needed */
            display: block;
            margin: 0 auto;
            /* Center align the button */
        }
        nav ul {
    list-style-type: none;
    padding: 0;
    }

    nav ul li {
        display: inline-block;
        margin-right: 10px;
    }

    nav ul li a {
        text-decoration: none;
        color: inherit;
        padding: 5px 10px;
        border-radius: 5px;
        display: flex; /* Add this property to enable flexbox */
        align-items: center; /* Align items vertically in the center */
    }

    nav ul li a .icon {
    margin-right: 10px; /* Adjust margin as needed */
    }

    nav ul li a:hover {
        background-color: #eee;
    }

    nav ul li a.active {
        background-color: #ccc;
    }


        /* Responsive styles using media queries */
        @media (max-width: 768px) {
            .container {
                flex-direction: column;
                align-items: center;
            }

            .sidebar {
                width: 100%;
                border-radius: 0;
                margin-bottom: 20px;
            }

            .main-content {
                padding-left: 0;
            }
        }

        @media (max-width: 600px) {
            th,
            td {
                font-size: 12px;
            }

            button {
                font-size: 12px;
                padding: 5px;
                /* Adjust padding for smaller screens */
            }
        }

        @media (max-width: 400px) {
            th,
            td {
                font-size: 10px;
            }

            button {
                font-size: 10px;
                padding: 3px;
                /* Adjust padding for smaller screens */
            }
        }
    </style>
</head>

<body>

    <div class="container">
        <div class="sidebar">
        <div class="container" style=" background-color: #fff;
            border-top-right-radius: 40px;
            border-bottom-right-radius: 40px;
            padding: 20px;
            display: flex;
            flex-direction: column;
            align-items: center;
            width: 400px;">
<img src="../../image/logo.png" alt="createproject" style="max-width: 100%;height: auto;width: 100px;border-radius: 10px;"/>
<div style="text-align: center; display: flex;
            flex-direction: column;">
    <h5 style="margin-top: 10px;">
    @auth
        <p>Welcome <br>{{ auth()->user()->name }}</p>
    @endauth
    </h5>
        
    <h5>{{ auth()->user()->role }}</h5>
</div>

<nav  style="text-align: center; display: flex;
            flex-direction: column;">
    <ul  style="text-align: center; display: flex;
            flex-direction: column;">
        <li>
    <a href="{{ route('student.studentDashboard', ['id' => $id]) }}" class="{{ request()->routeIs('student.studentDashboard') ? 'active' : '' }}">
        <i class="fas fa-home"></i> Dashboard
    </a>
</li>

        <li>
            <a href="{{ route('logout') }}"  class="{{ request()->routeIs('student.studentDashboard') ? 'active' : '' }}" >
                <i class="fas fa-sign-out-alt"></i> LogOut
            </a>
        </li>
    </ul>
</nav>
    </div>
            <div style="margin-top: 20px; text-align: center; background-color: #ff7900; padding: 20px; border-radius: 5px; border-bottom-right-radius: 20px;">
        <h5>College Information</h5>
        <p>Gyalpozhing College of Information Technology</p>
        <p>Royal University of Bhutan</p>
        <p>Chamjekha, Thimphu, Bhutan</p>
        <hr>
        <p style="color: #fff;">Copyright © Gyalpozhing College of Information Technology 2023. All Rights Reserved.</p>
    </div>
        </div>

        <div class="main-content">
            <h5 style="color: #000000; padding-bottom: 20px;"><i class="fas fa-home"></i> Dashboard</h5>
            <div class="project-status">
                <table>
                    <tbody><tr>
                        <th>Sl.No</th>
                        <th>Task</th>
                        <th>Marks</th>
                    </tr>
                    <tr class="data-row">
                        <td>1</td>
                        <td>CA1</td>
                        <td><a href="http://127.0.0.1:8000/student/viewMarks/6/CA1" class="view-button" style="text-decoration: none; color: black; font-weight: bold;">Download CA1</a></td>
                    </tr>
                    <tr class="data-row">
                        <td>2</td>
                        <td>CA2</td>
                        <td><a href="http://127.0.0.1:8000/student/viewMarks/6/CA2" class="view-button" style="text-decoration: none; color: black; font-weight: bold;">Download CA2</a></td>
                    </tr>
                    <tr class="data-row">
                        <td>3</td>
                        <td>CA3</td>
                        <td><a href="http://127.0.0.1:8000/student/viewMarks/6/CA3" class="view-button" style="text-decoration: none; color: black; font-weight: bold;">Download CA3</a></td>
                    </tr>
                    <tr class="data-row">
                        <td>4</td>
                        <td>CA4</td>
                        <td><a href="http://127.0.0.1:8000/student/viewMarks/6/CA4" class="view-button" style="text-decoration: none; color: black; font-weight: bold;">Download CA4</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <br>
        <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-indicators">
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
            </div>
            <div class="carousel-inner">
                <div class="carousel-item active">
                <img src="../../image/slide3.jfif" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <h5 >Gyelposhing College of Information Technology</h5>
                    <p>Kabisa</p>
                </div>
                </div>
                <div class="carousel-item">
                <img src="../../image/slide2.png" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <h5 style="color: black;">"Our vision is to be a leading institution in software technology and interactive design that produces future ready graduates with commitment to academic excellence, innovation, and social responsibility"
                    </h5>
                    
                </div>
                </div>
                <div class="carousel-item">
                <img src="../../image/slide1.png" class="d-block w-120" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <h5 style="color: black;">“Be a student as long as you still have something to learn, and this will mean all your life.” — Henry L. Doherty</h5>
                    
                </div>
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
            </div>
    </div>
    
</div>

</body>

</html>



@else
<div>
    Page not found!
</div>
@endif



