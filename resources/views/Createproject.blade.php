@if(session('role') === 'admin')

<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <link rel="stylesheet" href="{{ asset('vendor/sweetalert2/sweetalert2.min.css') }}">
<script src="{{ asset('vendor/sweetalert2/sweetalert2.all.min.js') }}"></script>

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
      crossorigin="anonymous"
    />
    <title>Document</title>
  </head>
  <body>
    <div class="container-fluid">
    @csrf
      <div class="row" style="background-color: #f2edf3">
        <div
          class="col-sm-3"
          style="
            border: 2px solid white;
            border-top-right-radius: 30px;
            border-bottom-right-radius: 30px;
            position: relative;
            max-height: 100%;
            max-width: auto;
            justify-content: center;
            align-items: center;
            display: flex;
            flex-direction: column;
            background-color: white;
            box-shadow: 10px 0 5px rgba(0, 0, 0, 0.2);
          "
        >
          <div
            class="d-flex flex-column"
            style="align-items: center; text-align: center"
          >
            <div style="max-width: 100px; height: auto; margin-top: 15%">
              <img
                src="./image/profile.png"
                alt="profileimage"
                style="max-width: 100%; height: auto"
              />
            </div>
                @auth
                <p>Welcome <br>{{ auth()->user()->name }}</p>
                @else
                <p>User not found</p>
                <a href="/login">Login</a>
                <p>Login in</p>
                @endauth
                <h5>Manager</h5>
          </div>

          <div
            class="d-flex flex-row"
            style="align-items: center; justify-content: center"
          >
            <div
              class="d--lg-flex flex-column"
              style="align-items: center; justify-content: center"
            >
              <a
              href="{{route('home')}}"
                style="
                  display: inline-block;
                  padding: 10px 40px;
                  background-color: #ff7900;
                  color: #fff;
                  text-decoration: none;
                  border-radius: 5px;
                  display: flex;
                  justify-content: space-around;
                  align-items: center;
                  margin: 10%;
                "
                ><img
                  src="./image/dashboard.png"
                  alt="createproject"
                  style="
                    max-width: 100%;
                    height: auto;
                    width: 40px;
                    border-radius: 10px;
                  "
                />
                <h5
                  style="
                    color: #149634;
                    font-size: 15px;
                    font-weight: bold;
                    margin: 0;
                  "
                >
                  Dashboard
                </h5>
              </a>
            </div>
          </div>

          <div
            style="
              align-items: center;
              justify-content: center;
              padding-top: 10%;
            "
          >
          <div class="d--lg-flex flex-column" style="align-items: center; justify-content: center; padding: 10px; text-align: center;">
              <img src="./image/createicon.png" alt="createproject" style="max-width: 100%; height: auto; width: 30px; border-radius: 10px;">
              <a href="{{route('Createproject')}}" style="display: inline-block; padding: 5px 10px; color: #ff7900; text-decoration: none; border-radius: 5px; transition: background-color 0.3s;" onmouseover="this.style.backgroundColor='#ff7900'; this.style.color='#fff';" onmouseout="this.style.backgroundColor=''; this.style.color='#ff7900';">
                  <p style="font-size: 16px; color: #149634; margin: 0;">Create Project</p>
              </a>
          </div>


            <div
              class="d--lg-flex flex-column"
              style="
                align-items: center;
                justify-content: center;
                padding: 10px;
                text-align: center;
              "
            >
              <img
                src="./image/assign.png"
                alt="createproject"
                style="
                  max-width: 100%;
                  height: auto;
                  width: 30px;

                  border-radius: 10px;
                "
              />
              <a
              href="{{route('AssignProject')}}"
              style="display: inline-block; padding: 5px 10px; color: #ff7900; text-decoration: none; border-radius: 5px; transition: background-color 0.3s;" onmouseover="this.style.backgroundColor='#ff7900'; this.style.color='#fff';" onmouseout="this.style.backgroundColor=''; this.style.color='#ff7900';"
                ><p
                  style="
                    font-size: 16px;
                    color: #149634;

                    margin: 0;
                  "
                >
                  Assign Project
                </p></a
              >
            </div>
            <div
              class="d--lg-flex flex-column"
              style="
                align-items: center;
                justify-content: center;
                padding: 10px;
                text-align: center;
              "
            >
              <img
                src="./image/group.png"
                alt="createproject"
                style="
                  max-width: 100%;
                  height: auto;
                  width: 30px;

                  border-radius: 10px;
                "
              />
              <a
              href="{{route('projectcartegorize')}}"
              style="display: inline-block; padding: 5px 10px; color: #ff7900; text-decoration: none; border-radius: 5px; transition: background-color 0.3s;" onmouseover="this.style.backgroundColor='#ff7900'; this.style.color='#fff';" onmouseout="this.style.backgroundColor=''; this.style.color='#ff7900';"
                ><p
                  style="
                    font-size: 16px;
                    color: #149634;

                    margin: 0;
                  "
                >
                  Project Group
                </p></a
              >
            </div>
            <div
              class="d--lg-flex flex-column"
              style="
                align-items: center;
                justify-content: center;
                padding: 10px;
                text-align: center;
              "
            >
              <img
                src="./image/createi.png"
                alt="createproject"
                style="
                  max-width: 100%;
                  height: auto;
                  width: 30px;

                  border-radius: 10px;
                "
              />
              <a
              href="{{route('CreateEvalution')}}"
              style="display: inline-block; padding: 5px 10px; color: #ff7900; text-decoration: none; border-radius: 5px; transition: background-color 0.3s;" onmouseover="this.style.backgroundColor='#ff7900'; this.style.color='#fff';" onmouseout="this.style.backgroundColor=''; this.style.color='#ff7900';"
                ><p
                  style="
                    font-size: 16px;
                    color: #149634;

                    margin: 0;
                  "
                >
                  Create Evaluation
                </p></a
              >
            </div>
            <div
              class="d--lg-flex flex-column"
              style="
                align-items: center;
                justify-content: center;
                padding: 10px;
                text-align: center;
              "
            >
              <img
                src="./image/archive.png"
                alt="createproject"
                style="
                  max-width: 100%;
                  height: auto;
                  width: 30px;

                  border-radius: 10px;
                "
              />
              <a
              href="{{route('archiveProject')}}"
              style="display: inline-block; padding: 5px 10px; color: #ff7900; text-decoration: none; border-radius: 5px; transition: background-color 0.3s;" onmouseover="this.style.backgroundColor='#ff7900'; this.style.color='#fff';" onmouseout="this.style.backgroundColor=''; this.style.color='#ff7900';"
                ><p
                  style="
                    font-size: 16px;
                    color: #149634;

                    margin: 0;
                  "
                >
                  Archive Project
                </p></a
              >
            </div>
          </div>
          <div style="padding-top: 10%; display: flex">
            <div
              class="d--lg-flex flex-column"
              style="
                align-items: center;
                justify-content: center;
                padding: 10px;
                text-align: center;
              "
            >
              <img
                src="./image/setting.png"
                alt="createproject"
                style="
                  max-width: 100%;
                  height: auto;
                  width: 20px;

                  border-radius: 10px;
                "
              />
              <a
                href="./Profile.html"
                style="display: inline-block; padding: 5px 10px; color: #ff7900; text-decoration: none; border-radius: 5px; transition: background-color 0.3s;" onmouseover="this.style.backgroundColor='#ff7900'; this.style.color='#fff';" onmouseout="this.style.backgroundColor=''; this.style.color='#ff7900';"
                ><p style="font-size: 16px; color: #149634; margin: 0">
                  Setting
                </p></a
              >
            </div>
            <div
              class="d--lg-flex flex-column"
              style="
                align-items: center;
                justify-content: center;
                padding: 10px;
                text-align: center;
              "
            >
              <img
                src="./image/logout.png"
                alt="createproject"
                style="
                  max-width: 100%;
                  height: auto;
                  width: 20px;

                  border-radius: 10px;
                "
              />
              <a
                href="{{route('logout')}}"
                style="display: inline-block; padding: 5px 10px; color: #ff7900; text-decoration: none; border-radius: 5px; transition: background-color 0.3s;" onmouseover="this.style.backgroundColor='#ff7900'; this.style.color='#fff';" onmouseout="this.style.backgroundColor=''; this.style.color='#ff7900';"
                ><p
                  style="
                    font-size: 16px;
                    color: #149634;

                    margin: 0;
                  "
                >
                  Log out
                </p></a
              >
             
            </div>
          </div>
          <div style="margin-top: 20px; text-align: center; background-color: #ff7900; padding: 10px; border-radius: 5px; border-bottom-right-radius: 20px;">
              <h5>College Information</h5>
              <p>Gyalpozhing College of Information Technology</p>
              <p>Royal University of Bhutan</p>
              <p>Chamjekha, Thimphu, Bhutan</p>
              <hr>
              <p style="color: #fff;">Copyright © Gyalpozhing College of Information Technology 2023. All Rights Reserved.</p>
          </div>

        </div>
        <div class="col-sm-9">
          <div>
            <div
              class="container"
              style="
                border: 2px solid white;
                margin-top: 10%;
                border-radius: 10px;
                background-color: white;
                box-shadow: 10px 10px 5px 5px rgba(0, 0, 0, 0.2);
              "
            >
              <div style="margin: 3%">
                <h4>Create Project</h4>
              </div>

              <div class="container" style="margin-top: 5%; margin-bottom: 2%">
                <div class="row align-items-start">
                  <div class="col" style="margin-left: 10%">
                  <!-- @if($message = Session::get("success"))
                  <div class = "alert alert-success alert-block">
                    <strong>{{$message}}</strong>
                  </div>
                  @endif -->
                  @include('sweetalert::alert')
                    <form action="{{ route('projectsdetail.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                      <div class="mb-3">
                          <label class="form-label" for="course">Course:</label>
                            <select  class="form-select" name="course" id="course" required>
                                <option value="" disabled selected>Select your course</option>
                                <option value="Bachelor Degree in Information Technology">Bachelor Degree in Information Technology</option>
                                <option value="Bachelor Degree in Computer science">Bachelor Degree in Computer science</option>
                                <option value="Blockchain Development">Blockchain Development</option>
                                <option value="Full Stack Development">Full Stack Development</option>
                                <option value="AI Development and Data science">AI Development and Data science</option>
                            </select>
                      </div>
                      <div class="mb-3">
                        <label class="form-label" for="projectgroup_no">project group No:</label>
                        <input type="text" class="form-control" value="{{old('projectgroup_no')}}"  name="projectgroup_no" id="projectgroup_no" required />
                        
                      </div>
                      <div class="mb-3">
                        <label class="form-label" for="project_title">Project Title:</label>
                        <input type="text" class="form-control" value="{{old('project_title')}}" name="project_title" id="project_title" required/>
                      </div>
                      <div class="mb-3">
                        <label class="form-label" for="product_owner">Product Owner:</label>
                        <input type="text" class="form-control" value="{{old('product_owner')}}" name="product_owner" id="product_owner" required/>
                      </div>
                      
                  
                  </div>
                  <div class="col-sm-5">
                    <div style="align-items: center; justify-content: center">
                      <div
                        class="d--lg-flex flex-column"
                        style="
                          align-items: center;
                          justify-content: center;
                          background-color: #f2edf3;
                          border-radius: 10px;
                          padding-left: 10px;
                          margin-top: 10%;
                          max-width: 80%;
                          height: auto;
                        "
                      >
                        <img
                          src="./image/upload.png"
                          alt="createproject"
                          style="
                            max-width: 100%;
                            height: auto;
                            width: 100px;
                            border-radius: 10px;
                          "
                        />
                        <!-- <a
                          href="./Createproject.html"
                          style="
                            display: inline-block;
                            padding: 10px 20px;
                            color: black;
                            text-decoration: none;
                            border-radius: 5px;
                            max-width: 100%;
                            height: auto;
                          "
                          >File Attachment</a
                        > -->
                        <label for="file">Upload File:</label>
                        <input type="file" name="file" id="file" required>
                        <p>
                      </div>
                    </div>
                  </div>
                  <div
                        class="d-flex flex-row"
                        style="
                          align-items: center;
                          justify-content: center;
                          margin-top: 10%;
                        "
                      >
                        <button
                          type="submit"
                          class="btn"
                          style="margin: 2%; background-color: #ff7900"
                        >
                          Create
                        </button>
                        <button
                          type="submit"
                          class="btn"
                          style="background-color: #ff7900"
                        ><a  href="{{route('Createproject')}}" style="color: black;text-decoration: none;">
                          Cancle</a>
                        </button>
                      </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>

@else
<div>Page not found!</div>
@endif
